<div align="center">
<h1>AnMa Centre de thérapies</h1>

[![pipeline status](https://gitlab.com/Daniel-Mendes/AnMaCentreDeTherapies/badges/master/pipeline.svg)](https://gitlab.com/Daniel-Mendes/AnMaCentreDeTherapies/-/commits/master)
[![coverage report](https://gitlab.com/Daniel-Mendes/AnMaCentreDeTherapies/badges/master/coverage.svg)](https://gitlab.com/Daniel-Mendes/AnMaCentreDeTherapies/-/commits/master)


  <br>
  <img src="public/images/logo.png" alt="Logo AnMa Centre de thérapies" width="200">
   <h3>[AnMa centre de thérapies](https://anmacentredetherapies.ch/): Je soulage vos tensions et douleurs articulaires à l'aide du massage</h3>
</div>

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
### Table des matières

- [Nom de domaine](#nom-de-domaine)
- [Technologies utilisées](#technologies-utilis%C3%A9es)
- [Dépendances](#d%C3%A9pendances)
- [Hébérgement](#h%C3%A9b%C3%A9rgement)
- [Police de caractère](#police-de-caract%C3%A8re)
- [Couleurs utilisées sur le site](#couleurs-utilis%C3%A9es-sur-le-site)
- [Installation](#installation)
- [Deployement](#deployement)
  - [On Realease branch](#on-realease-branch)
  - [On production server](#on-production-server)
- [Dossiers](#dossiers)
- [Variables d'environnement](#variables-denvironnement)
- [Créé par Daniel Mendes pour Anne-Marie Estoppey](#cr%C3%A9%C3%A9-par-daniel-mendes-pour-anne-marie-estoppey)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

### Nom de domaine

- [anmacentredetherapies.ch](https://anmacentredetherapies.ch/)

### Technologies utilisées

- [HTML5 / CSS3 / JS](https://devdocs.io/) - Language de base du web
- [Bootstrap](https://getbootstrap.com/docs/4.4/getting-started/introduction/) - The world’s most popular front-end open source toolkit
- [Font Awesome](https://fontawesome.com/how-to-use/on-the-web/referencing-icons/basic-use) - Obtenez des icônes vectorielles et des logos sociaux sur votre site web
- [Laravel](https://laravel.com/) - The PHP Framework for Web Artisans

### Dépendances

- [Noty](https://github.com/needim/noty) - Library that makes it easy to create alert - success - error - warning - information - confirmation messages as an alternative the standard alert dialog.
- [SmartPhoto.js](https://github.com/appleple/SmartPhoto) - The most easy to use responsive image viewer especially for mobile devices.
- [ScrollTrigger](https://github.com/terwanerik/ScrollTrigger) - Let your page react to scroll changes.

### Hébérgement

Le site web est hébérgé sur [infomaniak](https://www.infomaniak.com/fr).

- Hébérgement Web: 128.15 CHF/année TTC
- Nom de domaine: 8.87 CHF/année TTC

### Police de caractère

- Montserrat, sans-serif

### Couleurs utilisées sur le site

<table>
    <tr>
        <th>Beige</th>
        <th>Vert</th>
        <th>Rose</th>
    </tr>
    <tr style="color: #000000; font-weight: italic;">
        <td style="background-color: #e4d7c9;">#e4d7c9</td>
        <td style="background-color: #94a346;">#94a346</td>
        <td style="background-color: #ad5587;">#ad5587</td>
    </tr>
    <tr style="color: #000000; font-weight: italic;">
        <td style="background-color: #d9cdbf;">#D9CDBF</td>
        <td style="background-color: #828f40;">#828f40</td>
</table>

### Installation

Veuillez consulter le guide d'installation officiel de laravel pour connaître les exigences du serveur avant de commencer. [Documentation officielle](https://laravel.com/docs/6.x#installation)

Clonez le répertoire

    git clone https://gitlab.com/Daniel-Mendes/AnMaCentreDeTherapies.git

Passer au dossier du répertoire

    cd AnMaCentreDeTherapies

Installer toutes les dépendances à l'aide de [composer](https://getcomposer.org/doc/)

    composer install

Installer toutes les dépendances à l'aide de [npm](https://www.npmjs.com/get-npm)

    npm install

Copiez l'exemple du fichier env et apportez les modifications de configuration requises dans le fichier .env

    cp .env.example .env

Générer une nouvelle clé d'application

    php artisan key:generate

Démarrez le serveur de développement local

    php artisan serve

Vous pouvez maintenant accéder au serveur via <http://localhost:8000>

#### TL;DR liste de commandes

    git clone https://gitlab.com/Daniel-Mendes/AnMaCentreDeTherapies.git
    cd AnMaCentreDeTherapies
    composer install
    npm install
    cp .env.example .env
    php artisan key:generate

### Deployement

#### On Realease branch

    npm run production

#### On production server

    php artisan down --render="errors::maintenance" --retry=60 --secret="ab5z-t3a1-zyo4-om1n-l7f6"

    git pull

    composer install --optimize-autoloader --no-dev

    php artisan config:cache

    php artisan route:cache

    php artisan view:cache

    php artisan queue:work

    php artisan up

### Dossiers

- `app/Http/Controllers` - Contient tous les contrôleurs
- `app/Http/Requests` - Contient toutes les demandes de formulaire
- `config` - Contient tous les fichiers de configuration d'application
- `public/cockpit` - Contient tous les fichiers du CMS
- `resources/js` - Contient les fichiers JavaScript
- `resources/sass` - Contient les fichiers Sass
- `resources/views` - Contient les vues
- `routes/web.php` - Contient toutes les routes

### Variables d'environnement

- `.env` - Les variables d'environnement peuvent être définies dans ce fichier

### Créé par [Daniel Mendes](https://daniel-mendes.ch/) pour Anne-Marie Estoppey
