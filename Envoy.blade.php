@servers(['web' => 'n156t_deployer@n156t.ftp.infomaniak.com'])

@setup
    $repository = 'git@gitlab.com:Daniel-Mendes/AnMaCentreDeTherapies.git';
    $releases_dir = 'web/anmacentredetherapies/releases';
    $app_dir = 'web/anmacentredetherapies';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
    clone_repository
    run_composer
    update_symlinks
    optimization
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}
    composer install --optimize-autoloader --no-dev
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

    echo 'Linking cockpit directory'
    ln -nfs {{ $app_dir }}/cockpit {{ $new_release_dir }}/public/cockpit

    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
@endtask

@task('optimization')
    echo "Optimizing Configuration Loading"
    php artisan config:cache

    echo "Optimizing Route Loading"
    php artisan route:cache

    echo "Optimizing View Loading"
    php artisan view:cache
@endtask
