Vous avez reçu un nouveau message !
{{ $data['date'] }}


INFORMATIONS

Nom: {{ $data['nom'] }}
Prénom: {{ $data['prenom'] }}
Email: {{ $data['email'] }}
Téléphone: {{ $data['tel'] }}
Message:
{{ $data['message'] }}

© {{ date("Y") }} anmacentredetherapies.ch
