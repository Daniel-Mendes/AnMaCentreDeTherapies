<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Le massage relaxant du corps près de chez vous, n'hésitez pas à me contacter !">
        <meta name="keywords" content="massage, hypnose, jongny, vevey, montreux, pully, vaud, anne marie">
        <meta http-equiv="content-language" content="fr-CH">
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="author" content="Estoppey Anna Maria">
        <meta name="contact" content="anna1506@hotmail.ch">
        <meta name="copyright" content="© 2019 anmacentredetherapies.ch">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name') }}</title>
        <link rel="icon" type="image/gif" href="favicon.gif">
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    </head>
    <body data-spy="scroll" data-target="#navbarScrollSpy" data-offset="166">
        <div id="app">
            <nav class="navbar fixed-top navbar-expand-lg navbar-light">
                <div class="container-fluid mx-0 mx-md-5">
                    <a class="navbar-brand js-scroll-trigger" href="#top">
                        <img src="{{ asset('images/logo.png') }}" width="140px" class="d-inline-block align-top" alt="{{ config('app.name') }}">
                    </a>
                    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarLinks" aria-controls="navbarLinks" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span class="icon-bar top-bar"></span>
                        <span class="icon-bar middle-bar"></span>
                        <span class="icon-bar bottom-bar"></span>
                    </button>

                    <div class="collapse navbar-collapse h5 pl-2 pl-md-0 mb-0 pb-2" id="navbarLinks" data-slideInTop>
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav" id="navbarScrollSpy">
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="#a-propos">À propos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="#prestations">Prestations</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="#faq">FAQ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="#galerie">Galerie</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link d-none" href="#prendreRDV"></a>
                            </li>
                            <div class="underbar"></div>
                        </ul>
                    </div>
                </div>
            </nav>

            <main>
                @yield('content')
            </main>

            <footer>
                <div class="container-fluid text-center text-md-left bg-beige text-dark py-0" style="border-top: 1px solid rgba(0, 0, 0, 0.1);">
                    <div class="row text-center text-md-left mt-0 pt-3">
                        <div class="col mx-auto mt-3" data-slideInBottom>
                            <img src="{{ asset('images/logo.png') }}" width="100px" class="" alt="Logo">
                            <h6 class="mb-4 font-weight-light text-dark d-inline-block text-dark pl-2">{{ config('app.name') }}</h6>
                        </div>

                        <hr class="w-100 clearfix d-md-none">

                        <div class="col mx-auto mt-3" data-slideInBottom>
                            <h6 class="text-uppercase mb-4 font-weight-bold text-dark">Liens</h6>
                            <p class="text-dark">
                                <a href="#a-propos" class="text-decoration-none text-dark js-scroll-trigger">À propos</a>
                            </p>
                            <p>
                                <a href="#prestations" class="text-decoration-none text-dark js-scroll-trigger">Prestations</a>
                            </p>
                            <p>
                                <a href="#faq" class="text-decoration-none text-dark js-scroll-trigger">FAQ</a>
                            </p>
                            <p>
                                <a href="#galerie" class="text-decoration-none text-dark js-scroll-trigger">Galerie</a>
                            </p>
                            <p>
                                <a href="#contact" class="text-decoration-none text-dark js-scroll-trigger">Contact</a>
                            </p>
                        </div>

                        <hr class="w-100 clearfix d-md-none">

                        <div class="col mx-auto mt-3 text-wrap" data-slideInBottom>
                            <h6 class="text-uppercase mb-4 font-weight-bold text-dark">Contact</h6>
                            <p class="text-dark">
                                <i class="fas fa-home mr-3"></i>
                                <a href="https://s.geo.admin.ch/89fd41fbef" class="text-dark text-decoration-none" target="_blank" rel="noopener">Ch. de la Tuilière 2D, <span class='d-block d-xl-inline ml-0 ml-md-4 ml-xl-0 pl-0 pl-md-3 pl-xl-0'>1805 Jongny</span></a>
                            </p>
                            <p class="text-dark text-wrap">
                                <i class="fas fa-envelope mr-3"></i>
                                <a href="mailto:anmacentredetherapies@hotmail.ch" class="text-dark text-decoration-none">anmacentredetherapies@hotmail.com</a>
                            </p>
                            <p class="text-dark">
                                <i class="fas fa-phone mr-3"></i>
                                <a href="tel:+41797171296" class="text-dark text-decoration-none">+ 41 79 717 12 96</a>
                            </p>
                        </div>

                        <hr class="w-100 clearfix">
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <p class="text-center py-2 text-dark">
                                Photos prises par <a href="https://www.instagram.com/yani_gir_photographie/" class="text-dark" target="_blank" rel="noopener">Yani Girardier</a> <span class="d-block d-md-inline-block">&</span> <span class="d-block d-md-inline-block">Site web créé par <a href="https://daniel-mendes.ch/" class="text-dark" target="_blank" rel="noopener">Daniel Mendes</a></span>
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
            <a id="back-to-top" href="#top" class="btn btn-dark btn-lg js-scroll-trigger d-none" role="button" title="To the top page" data-toggle="tooltip" data-placement="left"><i class="fas fa-arrow-up"></i></a>
        </div>

        <!-- Scripts -->
        <script src="{{ mix('js/app.js') }}"></script>
        <!-- CDN -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" integrity="sha256-HAaDW5o2+LelybUhfuk0Zh2Vdk8Y2W2UeKmbaXhalfA=" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js" integrity="sha256-Y1rRlwTzT5K5hhCBfAFWABD4cU13QGuRN6P5apfWzVs=" crossorigin="anonymous"></script>
    </body>
</html>
