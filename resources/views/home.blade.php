@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <section class="row" id="top">
        <div class="jumbotron img-fluid jumbotron-fluid col-12 m-0 p-0">
            <div class="container-fluid pb-4" id="title-text">
                <div class="col-12 col-sm-10 col-lg-8 mx-auto py-3 text-center text-light" data-slideInBottom>
                    <h1 class="h1 mb-4">{{ config('app.name') }}</h1>
                    <h2 class="lead mb-3">Je soulage vos tensions et douleurs articulaires à l'aide du massage.</h2>
                    <h2 class="lead mb-4"> Je vous aide à renforcer vos propres capacités de changement, pour la réalisation de vos objectifs, à l'aide de l'hypnose.</h2>
                    <a class="btn btn-lg btn-prendreRDV js-scroll-trigger text-light" href="#prendreRDV" role="button">Prendre rendez-vous</a>
                </div>
            </div>
            <div class="container-fluid" id="chevron-down">
                <div class="col-12 mx-auto pb-4 px-0 text-center">
                    <i class="fas fa-chevron-down fa-2x"></i>
                </div>
            </div>
        </div>
    </section>

    <section class="row pb-5 bg-beige" id="a-propos">
        <div class="col-12">
            <h3 class="py-5 px-0 m-0 text-center text-uppercase h3-decoration" data-slideInBottom>À propos</h3>
            <div class="card bg-light border shadow col-12 col-sm-10 col-lg-8 mx-auto p-3 d-block" data-slideInBottom>
                <div class="row no-gutters">
                    <div class="col-12 col-lg-3 m-auto">
                        <img src="{{ asset("images/a-propos/avatar.png") }}" class="card-img img-fluid rounded" alt="Photo masseuse">
                    </div>
                    <div class="col-12 col-lg-9 my-auto">
                        <div class="card-body">
                            <div class="pb-5">
                                <h3 class="h4 card-title">{{ $about["name"] }}</h3>
                                <h4 class="h5 card-subtitle font-italic text-muted">{{ $about["certification"] }}</h4>
                            </div>
                            <p class="card-text lead">{!! parsedown($about["description"], true) !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="row pb-5" id="prestations">
        <div class="col-12 py-0">
            <h3 class="py-5 px-0 m-0 text-center col-12 text-uppercase h3-decoration" data-slideInBottom>Prestations</h3>
            <div class="accordion col-12 col-sm-10 col-lg-8 mx-auto p-0" id="accordionPrestations">
                @foreach ($prestations as $key => $prestation)
                @if ($loop->last)
                    <div class="card rounded shadow-sm border-0" data-slideInRight>
                @else
                    <div class="card rounded shadow-sm mb-3 border-0" data-slideInRight>
                @endif
                    <div class="card-header p-0 m-0 border-0" id="prestations-heading-{{ $key }}">
                        <h5 class="m-0 p-0">
                            <button class="btn text-decoration-none m-0 py-4 w-100" data-toggle="collapse" data-target="#prestations-collapse-{{ $key }}" aria-expanded="false" aria-controls="prestations-collapse-{{ $key }}">
                                <div class="row m-0 p-0">
                                    <div class="col-10 text-left accordion-text-active">
                                        <span class="prestations-title p-0 text-left">{{ $prestation["title"] }}</span>
                                        @if ($prestation["subtitle"])
                                            <span class="d-block d-lg-inline text-muted font-italic p-0 text-left text-justify"><span class="d-none d-lg-inline">-</span> {!! $prestation["subtitle"] !!}</span>
                                        @endif
                                    </div>
                                    <span class="accordion-icon fas fa-plus-circle text-right p-0 my-auto pr-4 col-2"></span>
                                </div>
                            </button>
                        </h5>
                    </div>
                    <div id="prestations-collapse-{{ $key }}" class="collapse" aria-labelledby="prestations-heading-{{ $key }}" data-parent="#accordionPrestations">
                        <div class="card-body">
                            <div class="mb-0 text-justify">{!! parsedown($prestation["description"]) !!}</div>
                            <hr class="w-100 clearfix">
                            <div class="row px-3">
                                <div class="d-inline-block p-0 col-3 col-md-1 h-100">
                                    <div>
                                        <p class="my-1 font-weight-bold d-inline-block">Durée</p>
                                    </div>
                                    <div>
                                        <p class="my-1 font-weight-bold d-inline-block">Prix</p>
                                    </div>
                                </div>
                                @foreach ($prestation["tarifs"] as $tarif)
                                    @if ($loop->iteration % 3 == 0)
                                    <div class="d-inline-block p-0 text-center col-4 col-md-2 offset-3 offset-md-0 pt-3 pt-md-0">
                                    @else
                                    <div class="d-inline-block p-0 text-center col-4 col-md-2">
                                    @endif
                                        <p class="my-1">{{ $tarif["value"]["time"] }}</p>
                                        <p class="mb-1 mt-2">{{ $tarif["value"]["price"] }} CHF</p>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="row justify-content-center pb-5 bg-beige" id="faq">
        <div class="col-12">
            <h3 class="py-5 m-0 text-center col-12 text-uppercase h3-decoration" data-slideInBottom>Questions / Réponses</h3>
            <div class="col-12 col-sm-10 col-lg-8 mx-auto p-0">
                @foreach ($faq as $key => $item)
                @if($loop->last)
                <div class="col-12 border-faq p-0" data-slideInRight>
                @else
                <div class="col-12 mb-4 border-faq p-0" data-slideInRight>
                @endif
                    <h4 class="h6 bg-light p-4 mb-0 font-weight-bold">{{ $item["question"] }}</h3>
                    <h5 class="h6 bg-white p-4 mb-0 text-muted">{!! parsedown($item["reponse"], true) !!}</h5>
                </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="row pb-5" id="galerie">
        <h3 class="col-12 py-5 m-0 text-center text-uppercase h3-decoration" data-slideInBottom>Galerie</h3>
        <div class="col-12 col-sm-10 col-lg-8 mx-auto">
            <div class="row row-cols-1 row-cols-md-2 row-cols-xl-3 px-2 px-xs-0">
                @foreach ($images["gallery"] as $image)
                    <a href="{{ $image["path"] }}" class="col p-1 m-0" style="height: 300px;" data-toggle="lightbox" data-gallery="photos-gallery" data-slideInBottom>
                        <img src="{{ $image["path"] }}" width="100%" height="100%"/>
                    </a>
                @endforeach
            </div>
        </div>
    </section>

    <section class="row justify-content-center pb-5 bg-beige" id="contact">
        <div class="col-12">
            <h3 class="pt-5 px-0 m-0 col-12 text-center text-uppercase h3-decoration" data-slideInBottom>Formulaire pour me contacter</h3>
            <div class="col-12 col-sm-10 col-lg-8 mx-auto container-noty px-0 py-4"></div>
            <form class="col-12 col-sm-10 col-lg-8 mb-0 mx-auto bg-light p-4 border rounded shadow" method="POST" action="{{ route('contact') }}" @submit.prevent="onSubmit" data-slideInBottom>
                @csrf
                @honeypot
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="nom">Nom</label>
                        <input type="text" class="form-control" v-bind:class="{ 'is-invalid': form.errors.get('nom') }" @keydown="form.errors.clear(['nom'])" id="nom" name="nom" v-model="form.nom" autocomplete="family-name">
                        <div class="invalid-feedback" v-if="form.errors.has('nom')" v-text="form.errors.get('nom')"></div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="prenom">Prénom</label>
                        <input type="text" class="form-control" v-bind:class="{ 'is-invalid': form.errors.get('prenom') }" @keydown="form.errors.clear(['prenom'])" id="prenom" name="prenom" v-model="form.prenom" autocomplete="given-name">
                        <div class="invalid-feedback" v-if="form.errors.has('prenom')" v-text="form.errors.get('prenom')"></div>
                    </div>
                </div>
                <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" v-bind:class="{ 'is-invalid': form.errors.get('email') }" @keydown="form.errors.clear(['email', 'tel'])" id="email" name="email" v-model="form.email" autocomplete="email">
                            <div class="invalid-feedback" v-if="form.errors.has('email')" v-text="form.errors.get('email')"></div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="tel">Téléphone</label>
                            <input type="tel" class="form-control" v-bind:class="{ 'is-invalid': form.errors.get('tel') }" @keydown="form.errors.clear(['tel', 'email'])" id="tel" name="tel" v-model="form.tel" autocomplete="tel">
                            <div class="invalid-feedback" v-if="form.errors.has('tel')" v-text="form.errors.get('tel')"></div>
                        </div>
                    </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="message">Message</label>
                        <textarea class="form-control" v-bind:class="{ 'is-invalid': form.errors.get('message') }" @keydown="form.errors.clear(['message'])" id="message" name="message" v-model="form.message" rows="5"></textarea>
                        <div class="invalid-feedback" v-if="form.errors.has('message')" v-text="form.errors.get('message')"></div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4 my-auto mb-0">
                        <button type="submit" class="btn btn-submit btn-block btn-lg text-light" :disabled="form.errors.any()">Envoyer</button>
                    </div>
                </div>
            </form>
        </div>
    </section>

    <section class="row pb-5 bg-green justify-content-around" id="prendreRDV">
        <h3 class="col-12 py-5 m-0 text-center text-uppercase text-light h3-decoration" data-slideInBottom>Prendre rendez-vous</h3>
        <div class="col-12 col-sm-10 col-lg-3 my-auto" data-slideInBottom>
            <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src='https://map.geo.admin.ch/embed.html?ch.bfs.gebaeude_wohnungs_register=280097122_0&time=None&lang=fr&topic=ech&bgLayer=ch.swisstopo.swissimage&layers=ch.bfs.gebaeude_wohnungs_register,ch.swisstopo.zeitreihen,ch.bav.haltestellen-oev,ch.swisstopo.swisstlm3d-wanderwege&layers_opacity=1,1,1,0.8&layers_visibility=true,false,false,false&layers_timestamp=,18641231,,&E=2554431.00&N=1148097.45&zoom=10' width='600' height='450' frameborder='0' style='border:0' title="map.geo.admin.ch"></iframe>
            </div>
        </div>
        <div class="col-12 col-sm-10 col-lg-8 pt-4 pt-lg-0" data-slideInBottom>
            <div class="embed-responsive embed-responsive-1by1" style="max-height: 600px;">
                <iframe class="embed-responsive-item" src="https://app.healthadvisor.ch/bookings/e4b805e27e3c03f7998b219ec4e1fb76?backUrl=https://anmacentredetherapies.ch/">
                    Désolé, votre navigateur ne supporte pas les iframes!
                </iframe>
            </div>
        </div>
    </section>
</div>
@endsection
