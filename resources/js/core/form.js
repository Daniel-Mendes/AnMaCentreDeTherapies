import Errors from './errors.js';
import Axios from 'axios';
import Noty from 'noty';

class Form {
    constructor(data) {
        this.originalData = data;

        for (let field in data) {
            this[field] = data[field];
        }

        this.errors = new Errors();
    }

    data() {
        let data = {};

        for (let property in this.originalData) {
            data[property] = this[property];
        }

        return data;
    }

    reset() {
        for (let field in this.originalData) {
            this[field] = '';
        }
    }

    submit(requestType, url) {
        Axios[requestType](url, this.data())
            .then(this.onSuccess.bind(this))
            .catch(this.onFail.bind(this));
    }

    onSuccess(response) {
        this.errors.clear();
        this.reset();
        new Noty({
            type: 'success',
            animation: {
                open: 'noty_effects_open',
                close: 'noty_effects_close'
            },
            text: response.data.message,
            timeout: 5000,
            progressBar: true,
            closeWith: ['click'],
            container: '.container-noty'
        }).show();
    }

    onFail(response) {
        this.errors.record(response.response.data.errors);
    }
}

export default Form;
