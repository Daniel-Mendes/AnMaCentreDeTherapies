class Errors {
    constructor() {
        this.errors = { };
    }

    get(field) {
        if (this.errors[field]) {
            return this.errors[field][0];
        }
    }

    has(field) {
        return Object.prototype.hasOwnProperty.call(this.errors, field);
    }

    any() {
        return Object.keys(this.errors).length > 0;
    }

    clear(fields) {
        if(fields) {
            fields.forEach(field => {
                delete this.errors[field];
            });
        } else {
            this.errors = {};
        }

    }

    record(errors) {
        this.errors = errors;
    }
}

export default Errors;
