$(document).ready(function () {
    let phone = window.matchMedia('(max-width: 768px)').matches;
    let tablet = window.matchMedia('(max-width: 992px)').matches;
    let laptop = window.matchMedia('(max-width: 1199px)').matches;
    let desktop = window.matchMedia('(min-width: 1200px)').matches;

    var navbarColors = function() {
        phone = window.matchMedia('(max-width: 768px)').matches;
        tablet = window.matchMedia('(max-width: 992px)').matches;
        laptop = window.matchMedia('(max-width: 1199px)').matches;
        desktop = window.matchMedia('(min-width: 1200px)').matches;

        // Navbar link color
        if(laptop === true || desktop === true) {
            $('#navbarScrollSpy .nav-link').addClass('text-light');
        }
        // Navbar toggler link color
        if(phone === true || tablet === true) {
            $('#navbarLinks').on('show.bs.collapse', function () {
                $('#navbarScrollSpy .nav-link').removeClass('text-light');
                $('nav').addClass('navbar-shrink');
            });
            $('#navbarLinks').on('hide.bs.collapse', function () {
                if($('nav').offset().top < 60) {
                    $('nav').removeClass('navbar-shrink');
                }
            });
        }
    };
    // Check navbar colors when resize windows
    $(window).resize(navbarColors);
    // Check navbar colors now
    navbarColors();

    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('body, html').animate({
                    scrollTop: (target.offset().top - 165)
                }, 600, 'swing');
                return false;
            }
        }
    });
    // Closes responsive menu when a scroll trigger link is clicked
    $('.js-scroll-trigger').click(function() {
        $('nav').addClass('navbar-shrink');
        $('.navbar-collapse').collapse('hide');
    });

    // Collapse Navbar
    var navbarCollapse = function() {
        if ($('nav').offset().top > 60) {
            $('nav').addClass('navbar-shrink');
            if(laptop === true || desktop === true) {
                $('#navbarScrollSpy .nav-link').removeClass('text-light');
            }
        } else if ($('nav').offset().top < 60) {
            if (phone === true || tablet === true) {
                if ($('.navbar-toggler').hasClass('collapsed')) {
                    $('nav').removeClass('navbar-shrink');
                }
            } else {
                $('nav').removeClass('navbar-shrink');
                $('#navbarScrollSpy .nav-link').addClass('text-light');
            }
        }

        if ($('nav').offset().top > 634) {
            $('#back-to-top').addClass('btn-scroll-to-top');
        } else {
            $('#back-to-top').removeClass('btn-scroll-to-top');
        }
    };
    // Collapse the navbar when page is scrolled
    $(window).scroll(navbarCollapse);
    // Collapse now if page is not at top
    navbarCollapse();
    // Collapse the navbar when logo is pressed when top = 0
    $('.navbar-brand').click(navbarCollapse);

    
});
