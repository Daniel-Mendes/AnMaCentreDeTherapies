$(document).ready(function () {
    // This event fires immediately when the show instance method is called.
    $('.accordion').on('show.bs.collapse hidden.bs.collapse', function (e) {
        $(e.target.previousElementSibling).toggleClass('accordion-active');
        $(e.target.previousElementSibling).find('.row .fas').toggleClass('fa-plus-circle fa-minus-circle');
    });
});
