import ScrollTrigger from '@terwanerik/scrolltrigger';

$(document).ready(function() {
    const trigger = new ScrollTrigger();

    trigger.add('[data-slideInLeft]')
        .add('[data-slideInRight]')
        .add('[data-slideInBottom]')
        .add('[data-slideInTop]');
});