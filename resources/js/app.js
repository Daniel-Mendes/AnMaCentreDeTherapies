/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import './bootstrap';
import Vue from 'vue';

/**
 * Promise Polyfill
 */
import Promise from 'promise-polyfill';
import setAsap from 'setasap';
Promise._immediateFn = setAsap;

import Form from './core/form';
import './core/accordion';
import './core/navbar';
import './core/scrollTrigger';
import './core/lightbox';

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 */

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

new Vue({
    el: '#app',
    data: {
        form: new Form({
            nom: '',
            prenom: '',
            email: '',
            tel: '',
            message: ''
        })
    },
    methods: {
        onSubmit() {
            this.form.submit('post', '/contact');
            $('body, html').animate({
                scrollTop: ($('#contact').offset().top - 165)
            }, 600, 'swing');
        }
    }
});
