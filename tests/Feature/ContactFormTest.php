<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContactFormTest extends TestCase
{
    use WithFaker;

    /**
     * Test session has form errors.
     *
     * @return void
     */
    public function testAssertSessionHasValidationErrors()
    {
        $this->withoutMiddleware();
        
        $response = $this->postJson('/contact', [
            'email' => null,
            'message' => null,
            'nom' => null,
            'prenom' => null,
            'tel' => null
        ]);

        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'email' => [
                        "Un email ou un numéro de téléphone est requis"
                    ],
                    'message' => [
                        "Un message est requis"
                    ],
                    'nom' => [
                        "Un nom est requis"
                    ],
                    'prenom' => [
                        "Un prénom est requis"
                    ],
                    'tel' => [
                        "Un numéro de téléphone ou un email est requis"
                    ]
                ]
            ]);
    }

    /**
     * Test session has not from errors
     *
     */
    public function testAssertSessionHasNotValidationErrors()
    {
        $this->withoutMiddleware();

        $this->withExceptionHandling();
        
        $response = $this->postJson('/contact', [
            'email' => $this->faker->email,
            'message' => $this->faker->text(200),
            'nom' => $this->faker->lastName,
            'prenom' => $this->faker->firstName,
            'tel' => $this->faker->e164PhoneNumber
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => 'Le message a bien été envoyé, merci !'
            ]);
    }
}
