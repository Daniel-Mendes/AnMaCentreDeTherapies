# Release Notes

## [1.4.0 (2020-10-21)](https://gitlab.com/Daniel-Mendes/AnMaCentreDeTherapies/-/compare/v1.3.0...v1.4.0)

### Fixed
- Fixed h3 decoration

### Added
- Added Envoy deployment
- Added cobertura report

### Updated
- Updated packages

## [1.3.0 (2020-09-19)](https://gitlab.com/Daniel-Mendes/AnMaCentreDeTherapies/-/compare/v1.2.0...v1.3.0)

### Updated
- Migrated from Laravel 7.x to 8.x
- Updated cockpit from 0.10.2 to 0.11.2

## [1.2.0 (2020-09-19)](https://gitlab.com/Daniel-Mendes/AnMaCentreDeTherapies/-/compare/v1.1.1...v1.2.0)

### Fixed
- Top image not showing in chrome

### Added
- Added contact form test
- Added honey pot
- Added Renovate for dependencies updates on branch develop

### Changed
- Changed Guzzle to Http facade
- Changed map

## [1.1.1 (2020-05-28)](https://gitlab.com/Daniel-Mendes/AnMaCentreDeTherapies/-/compare/v1.1.0...v1.1.1)

### Fixed
- Contact form not being sent

### Updated
- Updated prestations accessibility with tab

## [1.1.0 (2020-05-28)](https://gitlab.com/Daniel-Mendes/AnMaCentreDeTherapies/-/compare/v1.0.3...v1.1.0)

### Added
- Added Sentry
- Added Cockpit CMS

### Changed
- Changed textarea min-height
- Changed links in footer

### Updated
- Updated phpstan level to 8
- Updated to laravel 7
- Updated accesibility of prestations with tab

### Fixed
- Fixed autofill background color

## [1.0.3 (2020-03-01)](https://gitlab.com/Daniel-Mendes/AnMaCentreDeTherapies/-/compare/v1.0.2...v1.0.3)

### Added
- Added .htacess protection

### Changed
- Changed mail address

## [1.0.2 (2020-03-01)](https://gitlab.com/Daniel-Mendes/AnMaCentreDeTherapies/-/compare/v1.0.1...v1.0.2)

### Added
- Added photographer's link to instagram

### Changed
- Change package for animation on scroll
- Change package for lightbox

### Fixed
- Corrected the spelling of the photographer's name

## [1.0.1 (2020-02-15)](https://gitlab.com/Daniel-Mendes/AnMaCentreDeTherapies/-/compare/v1.0.0...v1.0.1)

### Added
- Added link to mail and phone on mail
- Added automatic year on copyright

### Changed
- Fixed responsive navbar white background
- Fixed navbar when resize window
- Fixed footer mail on tablet overflow

## [1.0.0 (2020-02-12)](https://gitlab.com/Daniel-Mendes/AnMaCentreDeTherapies/-/commit/847bf0fde1221818055d63c29bb2c5bd2c5e14a0)

### Added
- First public release
