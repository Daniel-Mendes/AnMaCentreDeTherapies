#!/bin/bash

print_vulnerabilities () {
    jq .metadata.vulnerabilities < ./audit_result.json
    cat ./audit_result.json
}

npm audit
npm audit --json > audit_result.json

INFO_VUL="$(jq .metadata.vulnerabilities.info < ./audit_result.json)"
LOW_VUL="$(jq .metadata.vulnerabilities.low < ./audit_result.json)"
MODERATE_VUL="$(jq .metadata.vulnerabilities.moderate < ./audit_result.json)"
HIGH_VUL="$(jq .metadata.vulnerabilities.high < ./audit_result.json)"
CRITICAL_VUL="$(jq .metadata.vulnerabilities.critical < ./audit_result.json)"

if [ "$INFO_VUL" -ne "0" ]
then
    print_vulnerabilities
    exit 1
fi

if [ "$LOW_VUL" -ne "0" ]
then
    print_vulnerabilities
    exit 1
fi

if [ "$MODERATE_VUL" -ne "0" ]
then
    print_vulnerabilities
    exit 1
fi

if [ "$HIGH_VUL" -ne "0" ]
then
    print_vulnerabilities
    exit 1
fi

if [ "$CRITICAL_VUL" -ne "0" ]
then
    print_vulnerabilities
    exit 1
fi
