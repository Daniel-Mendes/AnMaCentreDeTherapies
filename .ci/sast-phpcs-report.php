<?php
include 'Sniffs.php';

$rawFileReport = file_get_contents(dirname(__FILE__) . '/../phpcs-report.json');

if ($rawFileReport === false) {
    die('file not found or error ocurred reading report');
}

$report = json_decode($rawFileReport, true);

if ($report === null) {
    die('report can not be decoded');
}
print "[";
$firstLine = true;
if (is_array($report['files'])) {
    foreach ($report['files'] as $phpcs_file => $phpcs_issues) {
        foreach ($phpcs_issues['messages'] as $phpcs_issue_data) {
            if (Sniffs::isValidIssue($phpcs_issue_data)) {
                $path = preg_replace('/^\/code\//', '', $phpcs_file);
                $check_name = str_replace('.', ' ', $phpcs_issue_data["source"]);
                $cleaned_single_issue = array(
                    'type' => 'issue',
                    'check_name' => $check_name,
                    'description' => $phpcs_issue_data['message'],
                    'categories' => array('Style'),
                    'location' => array(
                        'path' => $path,
                        'lines' => array(
                            'begin' => $phpcs_issue_data['line'],
                            'end' => $phpcs_issue_data['line']
                        )
                    ),
                    'remediation_points' => Sniffs::pointsFor($phpcs_issue_data),
                    'engine_name' => 'phpcodesniffer',
                    'fingerprint' => md5($path . $check_name . $phpcs_issue_data['line'])
                );
                if (!$firstLine) {
                    print ",";
                } else {
                    $firstLine = false;
                }
                print json_encode($cleaned_single_issue, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
            }
        }
    }
}
print "]";