<?php

namespace App\Console\Formatter;

use Symfony\Component\Console\Output\OutputInterface;

class SimpleFormatter implements FormatterInterface
{
    /**
     * Displays a security report as simple plain text.
     *
     * @param OutputInterface $output
     * @param string          $lockFilePath    The file path to the checked lock file
     * @param array           $vulnerabilities An array of vulnerabilities
     *
     * @phpstan-ignore-next-line
     */
    public function displayResults(OutputInterface $output, $lockFilePath, array $vulnerabilities)
    {
        $output->writeln(sprintf('Security Check Report: <comment>%s</>', realpath($lockFilePath)));

        $status = 'OK';
        $style = 'info';
        $count = 0;

        if (count($vulnerabilities)) {
            $count = count($vulnerabilities);
            $status = 'CRITICAL';
            $style = 'error';
        }

        $output->writeln(sprintf(
            '<%s>[%s] %d %s known vulnerabilities</>',
            $style,
            $status,
            $count,
            $count === 1 ? 'package has' : 'packages have'
        ));

        if ($count !== 0) {
            $output->write("\n");

            foreach ($vulnerabilities as $dependency => $issues) {
                $dependencyFullName = $dependency.' ('.$issues['version'].')';
                $output->writeln('<info>'.$dependencyFullName."\n".str_repeat(
                    '-',
                    strlen($dependencyFullName)
                )."</>\n");

                foreach ($issues['advisories'] as $details) {
                    $output->write(' * ');
                    if ($details['cve']) {
                        $output->write('<comment>'.$details['cve'].': </comment>');
                    }
                    $output->writeln($details['title']);

                    if ($details['link'] !== '') {
                        $output->writeln('   '.$details['link']);
                    }

                    $output->writeln('');
                }
            }
        }
    }
}
