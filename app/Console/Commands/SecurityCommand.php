<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use SensioLabs\Security\SecurityChecker;
use App\Console\Formatter\SimpleFormatter;

class SecurityCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'security:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks composer.lock for any vulnerabilities against the SensioLabs checker.';

    /**
     * @var \SensioLabs\Security\SecurityChecker
     */
    protected $checker;

    /**
     * SecurityCommand constructor.
     *
     * @param \SensioLabs\Security\SecurityChecker $checker
     */
    public function __construct(SecurityChecker $checker)
    {
        parent::__construct();

        $this->checker = $checker;
    }

    /**
     * Execute the command
     *
     * @return int
     */
    public function handle()
    {
        // get the path to composer.lock
        $composerLock = base_path('composer.lock');

        // and feed it into the SecurityChecker
        $checkResult = json_decode((string) $this->checker->check($composerLock), true);

        // then display it using the formatter provided for Symfony
        app(SimpleFormatter::class)->displayResults($this->getOutput(), $composerLock, $checkResult);

        return 0;
    }
}
