<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

// use App\Rules\Recaptcha;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom' => 'required|max:255',
            'prenom' => 'required|max:255',
            'email' => 'required_without:tel|nullable|email',
            'tel' => 'required_without:email|nullable',
            'message' => 'required|max:512',
            // 'g-recaptcha-response' => ['required', new Recaptcha]
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nom.required' => 'Un nom est requis',
            'prenom.required' => 'Un prénom est requis',
            'email.required' => 'Un email est requis',
            'email.required_without' => 'Un email ou un numéro de téléphone est requis',
            'tel.required' => 'Un numéro de téléphone est requis',
            'tel.required_without' => 'Un numéro de téléphone ou un email est requis',
            // 'tel.regex' => 'Le format du numéro de téléphone est invalide',
            'tel.size' => 'La taille du numéro de téléphone est invalide',
            'message.required'  => 'Un message est requis',
            // 'g-recaptcha-response.required' => 'La vérification du recaptcha a échoué'
        ];
    }
}
