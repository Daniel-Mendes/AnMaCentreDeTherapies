<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ContactRequest;

class HomeController extends Controller
{
    /**
     * Display the home page
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $prestations = $this->getCollectionEntries('prestations');
        $faq = $this->getCollectionEntries('faq');
        $gallery = $this->getSingletonData('gallery');
        $about = $this->getSingletonData('about');

        // Sort images base on path: ex. 1.jpeg is before 2.jpeg
        usort($gallery["gallery"], function ($compareImage, $comparedImage) {
            return strcmp($compareImage["path"], $comparedImage["path"]);
        });

        return response()
            ->view('home', [
                'prestations' => $prestations,
                'faq' => $faq,
                'images' => $gallery,
                'about' => $about,
            ]);
    }

    /**
     * Validate contact form request
     *
     * @param \App\Http\Requests\ContactRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function contact(ContactRequest $request)
    {
        Mail::to(config('mail.from.address'))->send(new ContactMail($request->all()));

        return response()->json(array('message' => 'Le message a bien été envoyé, merci !'));
    }

    /**
     * Get collection entries
     *
     * @param string $collection
     * @return Array
     */
    private function getCollectionEntries($collection)
    {
        $response = Http::withToken(config('cockpit.key'))
                        ->get(config('cockpit.uri') . "api/collections/get/{$collection}");

        $results = json_decode($response->body(), true);

        return $results['entries'];
    }

    /**
     * Get singleton data
     *
     * @param string $singleton
     * @return Array
     */
    private function getSingletonData($singleton)
    {
        $response = Http::withToken(config('cockpit.key'))
                        ->get(config('cockpit.uri') . "api/singletons/get/{$singleton}");

        $results = json_decode($response->body(), true);

        return $results;
    }
}
